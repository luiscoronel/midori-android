package org.midoribrowser.android

import android.app.ActionBar
import android.app.Activity
import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.*
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.webkit.DownloadListener
import android.webkit.URLUtil
import android.webkit.WebView
import android.widget.AutoCompleteTextView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.get
import org.midoribrowser.android.adblock.Adblocker
import org.midoribrowser.android.bookmarks.Bookmark
import org.midoribrowser.android.bookmarks.BookmarkActivity
import org.midoribrowser.android.history.HistoryActivity
import org.midoribrowser.android.tabs.Tab
import org.midoribrowser.android.tabs.TabInfo
import org.midoribrowser.android.tabs.TabListActivity
import kotlinx.android.synthetic.main.activity_tab.*
import kotlinx.android.synthetic.main.view_tab.*
import kotlinx.android.synthetic.main.view_tab.view.*
import kotlinx.android.synthetic.main.view_tab_adapter.*

class TabActivity : AppCompatActivity() {

    var forwardButton: MenuItem? = null
    var countButton: MenuItem? = null
    var mMode: String? =  "NORMAL"
    var isTrue: Boolean? = true
    private val CONTEXT_MENU_ID_DOWNLOAD_IMAGE = 1
    private val OPEN_NEW_TAB_IMAGE = 2
    private val OPEN_NEW_INC_TAB_IMAGE = 3

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Database.initDb(this)
        Adblocker.init(this)
        TabInfo.activity = this

        setContentView(R.layout.activity_tab)

        setSupportActionBar(toolbar)
        address_bar.setOnFocusChangeListener { view, b -> run {
            if (!view.hasFocus()) {
                hideKeyboard(view)
            }
        }}

        address_bar.setOnEditorActionListener { view, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                view.clearFocus()
                var query = view.text.toString()
                TabInfo.currentWebView().search(query)
                true
            } else {
                false
            }
        }

        address_bar.setupClearButtonWithAction()
        onDownloadComplete()
    }

    override fun onResume() {
        super.onResume()

        refreshTabs()
        TabInfo.setRegisterContextMenu()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (web_view != null){
            web_view.destroy()
        }
    }

    private fun hideKeyboard(view: View) {
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    private fun reloadCurrentTab() {
        val webView = TabInfo.currentWebView()
        webView.reload()
    }

    fun addTab(mMode: String?) {
        val tab = Tab(this)
        tab.web_view.loadHome()
        TabInfo.addTab(tab, mMode)

        refreshTabs()
    }

    private fun refreshTabs() {
        if (TabInfo.currentIndex == -1) {
            addTab(mMode)
        } else {
            val tab = TabInfo.currentTab()

            frame_layout.removeAllViews()
            frame_layout.addView(tab)

            countButton?.title = TabInfo.count().toString()

            refreshForwardButton()
        }
    }

    private fun refreshForwardButton() {
        if (TabInfo.currentWebView().canGoForward()) {
            forwardButton?.isEnabled = true
            forwardButton?.icon?.alpha = 255
        } else {
            forwardButton?.isEnabled = false
            forwardButton?.icon?.alpha = 25
        }
    }

    override fun onBackPressed() {
        val webView = TabInfo.currentWebView()
        if (webView.canGoBack()) {
            webView.goBack()
            refreshForwardButton()
        }
        else super.onBackPressed()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
            menuInflater.inflate(R.menu.tab_menu, menu)
            return super.onCreateOptionsMenu(menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        forwardButton = menu?.findItem(R.id.forward_button)
        refreshForwardButton()
        if (TabInfo.changeMode() == "NORMAL"){
            menu?.getItem(4)!!.isVisible = false
        }else{
            menu?.getItem(3)!!.isVisible = false
            menu.getItem(4).isVisible = true
            menu.getItem(5).isVisible = false
        }

        countButton = menu?.findItem(R.id.tab_list_button)
        countButton?.title = TabInfo.count().toString()
        countButton?.actionView?.setBackgroundResource(R.drawable.tab_list_button)

        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.forward_button -> {
                TabInfo.currentWebView().goForward()
                refreshForwardButton()
            }
            R.id.reload_button -> {
                reloadCurrentTab()
                return true
            }
            R.id.tab_list_button -> {
                val intent = Intent(this, TabListActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.open_new_tab -> {
                isTrue = true
                mMode = "NORMAL"
                addTab(mMode)
                TabInfo.setAppBarLayout()
                return true
            }
            R.id.new_incognito_tab -> {
                isTrue = false
                mMode = "INCOGNITO"
                if (mMode == "INCOGNITO" ){
                    addMode()
                }
                addTab(mMode)
                TabInfo.setAppBarLayout()
                return true
            }
            R.id.close_incognito_tab -> {
                val intent = Intent(this, TabActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.add_bookmark -> {
                val webView = TabInfo.currentWebView()
                val bookmark = Bookmark(webView.title, webView.url)
                AsyncTask.execute {
                    Database.db?.bookmarkDao()?.insert(bookmark)
                }
            }
            R.id.show_bookmarks -> {
                val intent = Intent(this, BookmarkActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.show_history -> {
                val intent = Intent(this, HistoryActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.download -> {
                val intent = Intent()
                intent.setAction(DownloadManager.ACTION_VIEW_DOWNLOADS)
                startActivity(intent)
            }
            R.id.settings_item -> {
                val intent = Intent(this, SettingsActivity::class.java)
                startActivity(intent)
            }
            R.id.about_item -> {
                val intent = Intent(this, AboutActivity::class.java)
                startActivity(intent)
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (ev?.action == MotionEvent.ACTION_DOWN) {
            val view = currentFocus
            if (view is AutoCompleteTextView) {
                val rect = Rect()
                view.getGlobalVisibleRect(rect)
                if (!rect.contains(ev.rawX.toInt(), ev.rawY.toInt())) {
                    view.clearFocus()
                }
            }
        }

        return super.dispatchTouchEvent(ev)
    }

    fun addMode(): String?{
        if (isTrue != false){
            return "NORMAL"
        }else{
            return "INCOGNITO"
        }
    }

    fun AutoCompleteTextView.setupClearButtonWithAction() {

        addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editable: Editable?) {
                val clearIcon = if (editable?.isNotEmpty() == true) R.drawable.ic_close_black_24dp else 0
                setCompoundDrawablesWithIntrinsicBounds(0, 0, clearIcon, 0)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit
        })

        setOnTouchListener(View.OnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_UP) {
                if (event.rawX >= (this.right - this.compoundPaddingRight)) {
                    this.setText("")
                    return@OnTouchListener true
                }
            }
            return@OnTouchListener false
        })
    }

    override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {
        super.onCreateContextMenu(menu, v, menuInfo)
        val result = TabInfo.currentWebView().hitTestResult
        val urlOriginal:String = TabInfo.currentWebView().originalUrl.toString()
        val title:String = TabInfo.currentWebView().title.toString()
        menu?.setHeaderTitle("Download")
        menu?.setHeaderTitle(urlOriginal)
        TabInfo.currentWebView().hitTestResult?.let{
            when(it.type){
                WebView.HitTestResult.IMAGE_TYPE,
                WebView.HitTestResult.SRC_IMAGE_ANCHOR_TYPE -> {
                    menu?.add(0,  CONTEXT_MENU_ID_DOWNLOAD_IMAGE, 0, R.string.download_image)?.setOnMenuItemClickListener {
                        val imgUrl = result.extra
                        if (URLUtil.isNetworkUrl(imgUrl) && URLUtil.isValidUrl(imgUrl)){
                            val request:DownloadManager.Request = DownloadManager.Request(Uri.parse(imgUrl))
                            request.allowScanningByMediaScanner()
                            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                            val downloadManager = getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
                            downloadManager.enqueue(request)
                        }
                        false
                    }
                    menu?.add(1, OPEN_NEW_TAB_IMAGE, 1, "Open New Tab")?.setOnMenuItemClickListener {
                        val url = result.extra
                        val pdf = result.type

                        if (URLUtil.isValidUrl(url) && URLUtil.isNetworkUrl(url)){
                            TabInfo.addTab(url.toString(), "NORMAL")
                            refreshTabs()
                        }
                        false
                    }
                    menu?.add(2, OPEN_NEW_INC_TAB_IMAGE, 2, "Open New Incognito Tab")?.setOnMenuItemClickListener {
                        val url = result.extra
                        if (URLUtil.isValidUrl(url) && URLUtil.isNetworkUrl(url)){
                            TabInfo.addTab(url.toString(), "INCOGNITO")
                            refreshTabs()
                        }
                        false
                    }
                }
                else -> Toast.makeText(this, "It is not a type of image allowed: ${it.type}", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun onDownloadComplete(){

        val onComplete = object: BroadcastReceiver(){
            override fun onReceive(context: Context?, intent: Intent?) {
                Toast.makeText(context, "File Downloaded", Toast.LENGTH_SHORT).show()
            }
        }
        registerReceiver(onComplete, IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE))
    }
}
