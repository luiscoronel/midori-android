package org.midoribrowser.android

import android.webkit.WebView

class WebChromeClient : android.webkit.WebChromeClient() {
    override fun onProgressChanged(view: WebView?, newProgress: Int) {
        super.onProgressChanged(view, newProgress)

        val webView = view as org.midoribrowser.android.WebView
        webView.progressBar?.progress = newProgress
    }
}