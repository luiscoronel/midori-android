package org.midoribrowser.android

import androidx.room.Database
import androidx.room.RoomDatabase
import org.midoribrowser.android.bookmarks.Bookmark
import org.midoribrowser.android.bookmarks.BookmarkDao
import org.midoribrowser.android.history.History
import org.midoribrowser.android.history.HistoryDao

@Database(entities = [Bookmark::class, History::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun bookmarkDao(): BookmarkDao
    abstract fun historyDao(): HistoryDao
}